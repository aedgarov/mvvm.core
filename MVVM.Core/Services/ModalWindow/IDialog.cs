﻿using System;
using System.Windows;

namespace MVVM.Core.Services.ModalWindow
{
    /// <summary>
    /// Interface that mirrors some necessary properties of <see cref="Window"/> class
    /// to take control over <see cref="Window"/> instance that implements this interface.
    /// </summary>
    public interface IDialog : IModalWindow
    {
        /// <summary>
        /// Gets or sets the dialog result value, which is the value that is returned from
        /// the <see cref="Window.ShowDialog()"/> method.
        /// </summary>
        bool? DialogResult { get; set; }

        /// <summary>
        /// Opens a window and returns only when the newly opened window is closed.
        /// </summary>
        /// <returns>
        /// A <see cref="Nullable{T}"/> value of type <see cref="bool"/> that specifies whether the activity
        /// was accepted (true) or canceled (false). The return value is the value of the
        /// <see cref="Window.DialogResult"/> property before a window closes.
        /// </returns>
        bool? ShowDialog();
    }
}