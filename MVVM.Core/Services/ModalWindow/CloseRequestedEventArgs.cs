﻿using System.ComponentModel;
using System.Windows;

namespace MVVM.Core.Services.ModalWindow
{
    /// <summary>
    /// Provides data for <see cref="IClosable.CloseRequested"/> event.
    /// </summary>
    public sealed class CloseRequestedEventArgs : CancelEventArgs
    {
        /// <summary>
        /// Initializes a new instance of <see cref="CloseRequestedEventArgs"/> class.
        /// </summary>
        /// <param name="dialogResult">
        /// <see cref="Window.DialogResult"/> value.
        /// </param>
        public CloseRequestedEventArgs(bool? dialogResult)
            => DialogResult = dialogResult;

        /// <summary>
        /// Represents <see cref="Window.DialogResult"/> property.
        /// </summary>
        public bool? DialogResult { get; }
    }
}