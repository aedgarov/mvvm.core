﻿using System.Windows;

namespace MVVM.Core.Services.ModalWindow
{
    /// <summary>
    /// Interface that mirrors some necessary properties of <see cref="Window"/> class
    /// to take control over <see cref="Window"/> instance that implements this interface.
    /// </summary>
    public interface IModalWindow
    {
        /// <summary>
        /// Gets or sets the <see cref="Window"/> that owns this <see cref="Window"/>.
        /// </summary>
        Window Owner { get; set; }

        /// <summary>
        /// Gets or sets the suggested height of the element.
        /// </summary>
        double Height { get; set; }

        /// <summary>
        /// Gets or sets the width of the element.
        /// </summary>
        double Width { get; set; }

        /// <summary>
        /// Gets or sets the data context for an element when it participates in data binding.
        /// </summary>
        object DataContext { get; set; }

        /// <summary>
        /// Manually closes a <see cref="Window"/>.
        /// </summary>
        void Close();
    }
}