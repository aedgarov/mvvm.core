﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace MVVM.Core.Base
{
    /// <summary>
    /// Provides base functionality for implementing MVVM pattern with modal windows showing/closing and property validation capabilities.
    /// </summary>
    public abstract class ValidatableModalViewModelBase : ModalViewModelBase, INotifyDataErrorInfo
    {
        private readonly object _syncRoot = new object();
        private readonly ConcurrentDictionary<string, IList<string>> _errors;

        /// <summary>
        /// Initializes a new instance of <see cref="ValidatableModalViewModelBase"/> class.
        /// </summary>
        /// <param name="validateOnInit">
        /// Indicates whether validation shoud be performed on ViewModel initialization or not.
        /// </param>
        protected ValidatableModalViewModelBase(bool validateOnInit = true)
        {
            _errors = new ConcurrentDictionary<string, IList<string>>();

            if (validateOnInit)
                Task.WaitAll(ValidateAsync());
        }

        /// <summary>
        /// Indicates whether there are any errors in current ViewModel instance.
        /// </summary>
        public bool HasErrors
            => _errors.Any(error => error.Value != null && error.Value.Count > 0);

        /// <summary>
        /// Gets the list of all errors of specified property.
        /// </summary>
        /// <param name="propertyName">
        /// Name of property to check for errors.
        /// </param>
        /// <returns>
        /// List of all errors for specified property.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// Throws when <paramref name="propertyName"/> is null.
        /// </exception>
        public IEnumerable GetErrors(string propertyName)
        {
            _errors.TryGetValue(propertyName, out var currentErrors);
            return currentErrors;
        }

        /// <summary>
        /// Asynchronously validates all properties of the ViewModel.
        /// </summary>
        /// <returns>
        /// A task that represents the asynchronous validation operation.
        /// </returns>
        protected async Task ValidateAsync()
            => await Task.Run(() => Validate());

        /// <summary>
        /// Validates all properties of the ViewModel.
        /// </summary>
        protected void Validate()
        {
            lock (_syncRoot)
            {
                var validationContext = new ValidationContext(this, null, null);
                var validationResults = new List<ValidationResult>();
                Validator.TryValidateObject(this, validationContext, validationResults, true);

                foreach (var error in _errors.ToList())
                {
                    _errors.TryRemove(error.Key, out _);
                    OnErrorsChanged(error.Key);
                }

                var properties = validationResults
                    .SelectMany(result => result.MemberNames, (Result, Member) => new { Result, Member })
                    .GroupBy(entry => entry.Member, entry => entry.Result);

                foreach (var property in properties)
                {
                    var messages = property.Select(validation => validation.ErrorMessage).ToList();

                    if (_errors.ContainsKey(property.Key))
                        _errors.TryRemove(property.Key, out _);

                    _errors.TryAdd(property.Key, messages);
                    OnErrorsChanged(property.Key);
                }
            }
        }

        /// <summary>
        /// Sets the value into a property and raises <see cref="INotifyPropertyChanged.PropertyChanged"/> event, then validates ViewModel.
        /// </summary>
        /// <typeparam name="T">
        /// Property type.
        /// </typeparam>
        /// <param name="target">
        /// Value container.
        /// </param>
        /// <param name="value">
        /// Value to set.
        /// </param>
        /// <param name="propertyName">
        /// Property name, that is being changed.
        /// </param>
        /// <returns>
        /// True - if property has been changed, otherwise - false.
        /// </returns>
        protected override bool SetValue<T>(ref T property, T value, [CallerMemberName] string propertyName = null, ILogger logger = null)
        {
            var result = base.SetValue(ref property, value, propertyName, logger);
            Task.WaitAll(ValidateAsync());
            return result;
        }

        /// <summary>
        /// Invokes a <see cref="ErrorsChanged"/> event.
        /// </summary>
        /// <param name="propertyName">
        /// Name of the property whose errors have changed.
        /// </param>
        protected void OnErrorsChanged(string propertyName)
            => ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));

        /// <summary>
        /// Occurs when ViewModel errors changes.
        /// </summary>
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
    }
}