﻿using MVVM.Core.Base;
using System;
using System.Collections.Generic;

namespace MVVM.Core.Services.ModalWindow
{
    /// <summary>
    /// Service that provides ViewModel-View mappings and showing application views to follow MVVM pattern.
    /// </summary>
    public interface IModalWindowService
    {
        /// <summary>
        /// Contains ViewModel-View mappings, where Key is ViewModel type and Value is View type.
        /// </summary>
        IDictionary<Type, Type> Mappings { get; }

        /// <summary>
        /// Creates a new ViewModel-View mapping in <see cref="Mappings"/>.
        /// </summary>
        /// <typeparam name="TViewModel">
        /// ViewModel type.
        /// </typeparam>
        /// <typeparam name="TView">
        /// View type.
        /// </typeparam>
        void Register<TViewModel, TView>()
            where TViewModel : ModalViewModelBase
            where TView : IModalWindow;

        /// <summary>
        /// Invokes a <see cref="Window.ShowDialog()"/> method on a newly created
        /// <see cref="Window"/> instance based on <see cref="Mappings"/>.
        /// </summary>
        /// <typeparam name="TParentViewModel">
        /// Parent ViewModel type, to get the owning window.
        /// </typeparam>
        /// <typeparam name="TViewModel">
        /// ViewModel type.
        /// </typeparam>
        /// <param name="viewModel">
        /// Dialog window ViewModel.
        /// </param>
        /// <param name="height">
        /// Dialog window height.
        /// </param>
        /// <param name="width">
        /// Dialog window width.
        /// </param>
        /// <returns>
        /// A System.Nullable`1 value of type System.Boolean that specifies
        /// whether the activity was accepted (true) or canceled (false).
        /// </returns>
        bool? ShowDialog<TParentViewModel, TViewModel>(TViewModel viewModel, double height = double.NaN, double width = double.NaN)
            where TParentViewModel : ViewModelBase
            where TViewModel : ModalViewModelBase;

        /// <summary>
        /// Invokes a <see cref="Window.Show()"/> method on a newly created
        /// <see cref="Window"/> instance based on <see cref="Mappings"/>.
        /// </summary>
        /// <typeparam name="TParentViewModel">
        /// Parent ViewModel type, to get the owning window.
        /// </typeparam>
        /// <typeparam name="TViewModel">
        /// ViewModel type.
        /// </typeparam>
        /// <param name="viewModel">
        /// Window ViewModel.
        /// </param>
        /// <param name="height">
        /// Window height.
        /// </param>
        /// <param name="width">
        /// Window width.
        /// </param>
        void Show<TParentViewModel, TViewModel>(TViewModel viewModel, double height = double.NaN, double width = double.NaN)
            where TParentViewModel : ViewModelBase
            where TViewModel : ModalViewModelBase;
    }
}