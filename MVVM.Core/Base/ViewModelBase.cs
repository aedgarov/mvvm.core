﻿using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MVVM.Core.Base
{
    /// <summary>
    /// Provides base functionality for implementing MVVM pattern.
    /// </summary>
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        /// <summary>
        /// Sets the value into a property and raises <see cref="PropertyChanged"/> event.
        /// </summary>
        /// <typeparam name="T">
        /// Property type.
        /// </typeparam>
        /// <param name="target">
        /// Value container.
        /// </param>
        /// <param name="value">
        /// Value to set.
        /// </param>
        /// <param name="propertyName">
        /// Property name, that is being changed.
        /// </param>
        /// <param name="logger">
        /// Logger.
        /// </param>
        /// <returns>
        /// True - if property has been changed, otherwise - false.
        /// </returns>
        protected virtual bool SetValue<T>(ref T target, T value, [CallerMemberName] string propertyName = null, ILogger logger = null)
        {
            if (EqualityComparer<T>.Default.Equals(target, value)) return false;

            target = value;
            OnPropertyChanged(propertyName);
            logger?.LogInformation($"Property '{propertyName}' is set to '{value}'.");
            return true;
        }

        /// <summary>
        /// Invokes a <see cref="PropertyChanged"/> event.
        /// </summary>
        /// <param name="propertyName">
        /// Property name, that has been changed.
        /// </param>
        protected void OnPropertyChanged(string propertyName)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
    }
}