﻿using Microsoft.Extensions.Logging;
using System;
using System.Windows.Input;

namespace MVVM.Core.Actions
{
    public sealed class Command : ICommand
    {
        private readonly ILogger _logger;
        private readonly Action<object> _action;
        private readonly Predicate<object> _predicate;

        /// <summary>
        /// Initializes a new instance of <see cref="Command"/> class.
        /// </summary>
        /// <param name="action">
        /// Command action.
        /// </param>
        /// <param name="logger">
        /// Logger.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// Throws when <paramref name="action"/> is null.
        /// </exception>
        public Command(Action<object> action, ILogger logger = null) : this(action, null, logger) { }

        /// <summary>
        /// Initializes a new instance of <see cref="Command"/> class.
        /// </summary>
        /// <param name="action">
        /// Command action.
        /// </param>
        /// <param name="predicate">
        /// Command predicate.
        /// </param>
        /// <param name="logger">
        /// Logger.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// Throws when <paramref name="action"/> is null.
        /// </exception>
        public Command(Action<object> action, Predicate<object> predicate, ILogger logger = null)
        {
            _action = action ?? throw new ArgumentNullException(nameof(action));
            _predicate = predicate;
            _logger = logger;
        }

        /// <summary>
        /// Defines the method that determines whether the command can execute in its current state.
        /// </summary>
        /// <param name="parameter">
        /// Data used by the command. If the command does not require data to be passed,
        /// this object can be set to null.
        /// </param>
        /// <returns>
        /// True if this command can be executed, otherwise - false.
        /// </returns>
        public bool CanExecute(object parameter)
            => _predicate == null || _predicate(parameter);

        /// <summary>
        /// Defines the method to be called when the command is invoked.
        /// </summary>
        /// <param name="parameter">
        /// Data used by the command. If the command does not require data to be passed,
        /// this object can be set to null.
        /// </param>
        public void Execute(object parameter)
        {
            _action(parameter);
            _logger?.LogInformation($"Command executed with parameter '{parameter}'.");
        }

        /// <summary>
        /// Occurs when changes occur that affect whether or not the command should execute.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }
    }
}