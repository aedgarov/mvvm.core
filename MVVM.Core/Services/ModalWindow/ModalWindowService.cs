﻿using MVVM.Core.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace MVVM.Core.Services.ModalWindow
{
    /// <summary>
    /// Service that provides ViewModel-View mappings and showing application views to follow MVVM pattern.
    /// </summary>
    public sealed class ModalWindowService : IModalWindowService
    {
        private readonly Window _genericOwner;

        /// <summary>
        /// Initializes a new instance of <see cref="ModalWindowService"/> class.
        /// </summary>
        /// <param name="genericOwner"></param>
        public ModalWindowService(Window genericOwner)
            => _genericOwner = genericOwner;

        /// <summary>
        /// Contains ViewModel-View mappings, where Key is ViewModel type and Value is View type.
        /// </summary>
        public IDictionary<Type, Type> Mappings { get; } = new Dictionary<Type, Type>();

        /// <summary>
        /// Creates a new ViewModel-View mapping in <see cref="Mappings"/>.
        /// </summary>
        /// <typeparam name="TViewModel">
        /// ViewModel type.
        /// </typeparam>
        /// <typeparam name="TView">
        /// View type.
        /// </typeparam>
        /// <exception cref="ArgumentException">
        /// Throws when <see cref="Mappings"/> already contains key <typeparamref name="TViewModel"/>.
        /// </exception>
        public void Register<TViewModel, TView>()
            where TViewModel : ModalViewModelBase
            where TView : IModalWindow
        {
            if (Mappings.ContainsKey(typeof(TViewModel)))
                throw new ArgumentException($"Type {typeof(TViewModel)} is already mapped.");

            Mappings.Add(typeof(TViewModel), typeof(TView));
        }

        /// <summary>
        /// Invokes a <see cref="Window.Show()"/> method on a newly created
        /// <see cref="Window"/> instance based on <see cref="Mappings"/>.
        /// </summary>
        /// <typeparam name="TParentViewModel">
        /// Parent ViewModel type, to get the owning window.
        /// </typeparam>
        /// <typeparam name="TViewModel">
        /// ViewModel type.
        /// </typeparam>
        /// <param name="viewModel">
        /// Window ViewModel.
        /// </param>
        /// <param name="height">
        /// Window height.
        /// </param>
        /// <param name="width">
        /// Window width.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// Throws when <paramref name="viewModel"/> is null.
        /// </exception>
        public void Show<TParentViewModel, TViewModel>(TViewModel viewModel, double height = double.NaN, double width = double.NaN)
            where TParentViewModel : ViewModelBase
            where TViewModel : ModalViewModelBase
        {
            if (viewModel == null)
                throw new ArgumentNullException(nameof(viewModel));

            var viewModelType = typeof(TViewModel);

            if (!Mappings.ContainsKey(viewModelType))
                return;

            var modal = GetModalWindow(Mappings[viewModelType], height, width);

            if (modal is IWindow window)
            {
                void handler(object sender, CloseRequestedEventArgs e)
                {
                    viewModel.OnClosing(sender, e);
                    if (e.Cancel)
                        return;

                    viewModel.CloseRequested -= handler;
                    window.Close();
                }

                viewModel.CloseRequested += handler;
                window.DataContext = viewModel;
                window.Owner = GetOwner<TParentViewModel>() ?? _genericOwner;

                window.Show();
            }
        }

        /// <summary>
        /// Invokes a <see cref="Window.ShowDialog()"/> method on a newly created
        /// <see cref="Window"/> instance based on <see cref="Mappings"/>.
        /// </summary>
        /// <typeparam name="TParentViewModel">
        /// Parent ViewModel type, to get the owner window.
        /// </typeparam>
        /// <typeparam name="TViewModel">
        /// ViewModel type.
        /// </typeparam>
        /// <param name="viewModel">
        /// Dialog window ViewModel.
        /// </param>
        /// <param name="height">
        /// Dialog window height.
        /// </param>
        /// <param name="width">
        /// Dialog window width.
        /// </param>
        /// <returns>
        /// A <see cref="Nullable{T}"/> value of type <see cref="bool"/> that specifies
        /// whether the activity was accepted (true) or canceled (false).
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// Throws when <paramref name="viewModel"/> is null.
        /// </exception>
        public bool? ShowDialog<TParentViewModel, TViewModel>(TViewModel viewModel, double height = double.NaN, double width = double.NaN)
            where TParentViewModel : ViewModelBase
            where TViewModel : ModalViewModelBase
        {
            if (viewModel == null)
                throw new ArgumentNullException(nameof(viewModel));

            var viewModelType = typeof(TViewModel);

            if (!Mappings.ContainsKey(viewModelType))
                return null;

            var modal = GetModalWindow(Mappings[viewModelType], height, width);

            if (modal is IDialog dialog)
            {
                void handler(object sender, CloseRequestedEventArgs e)
                {
                    viewModel.OnClosing(sender, e);
                    if (e.Cancel)
                        return;

                    viewModel.CloseRequested -= handler;

                    if (e.DialogResult.HasValue) dialog.DialogResult = e.DialogResult;
                    else dialog.Close();
                }

                viewModel.CloseRequested += handler;
                dialog.DataContext = viewModel;
                dialog.Owner = GetOwner<TParentViewModel>() ?? _genericOwner;

                return dialog.ShowDialog();
            }

            return null;
        }

        /// <summary>
        /// Creates instance of the View, that inherits <see cref="IModalWindow"/>.
        /// </summary>
        /// <param name="modalWindowType">
        /// View type.
        /// </param>
        /// <param name="height">
        /// View height.
        /// </param>
        /// <param name="width">
        /// View width.
        /// </param>
        /// <returns>
        /// Instance of the View, that inherits <see cref="IModalWindow"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// Throws when <paramref name="modalWindowType"/> is null.
        /// </exception>
        private static IModalWindow GetModalWindow(Type modalWindowType, double height = double.NaN, double width = double.NaN)
        {
            if (modalWindowType == null)
                throw new ArgumentNullException(nameof(modalWindowType));

            var window = (IModalWindow)Activator.CreateInstance(modalWindowType);

            SizeToContent? sizeParameter = null;

            if (IsValidDouble(height))
                window.Height = height;
            else
                sizeParameter = SizeToContent.Height;

            if (IsValidDouble(width))
            {
                window.Width = width;
            }
            else
            {
                sizeParameter = sizeParameter == SizeToContent.Height
                    ? SizeToContent.WidthAndHeight
                    : SizeToContent.Width;
            }

            if (sizeParameter.HasValue && window is Window w)
                w.SizeToContent = sizeParameter.Value;

            return window;
        }

        /// <summary>
        /// Get the owner of the window which ViewModel type is equal to the specified type.
        /// </summary>
        /// <typeparam name="TViewModel">
        /// ViewModel type.
        /// </typeparam>
        /// <returns>
        /// <see cref="Window"/> instance that is owning View with the specified ViewModel type
        /// or <see langword="null"/> if the is no such View.
        /// </returns>
        private static Window GetOwner<TViewModel>()
            => Application.Current.Windows.OfType<Window>()
            .Where(window => window.DataContext?.GetType() == typeof(TViewModel))
            .FirstOrDefault();

        /// <summary>
        /// Verifies that the input double value is not <see cref="double.NaN"/>,
        /// <see cref="double.PositiveInfinity"/> or <see cref="double.NegativeInfinity"/>.
        /// </summary>
        /// <param name="value">
        /// Double value to verify.
        /// </param>
        /// <returns>
        /// True if double is not <see cref="double.NaN"/>, <see cref="double.PositiveInfinity"/>
        /// or <see cref="double.NegativeInfinity"/>, otherwise - false.
        /// </returns>
        private static bool IsValidDouble(double value)
            => !double.IsNaN(value) && !double.IsInfinity(value);
    }
}