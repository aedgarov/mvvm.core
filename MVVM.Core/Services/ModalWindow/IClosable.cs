﻿using System;

namespace MVVM.Core.Services.ModalWindow
{
    /// <summary>
    /// Provides a mechanism for closing View that implemented <see cref="IWindow"/> or <see cref="IDialog"/>
    /// from a ViewModel that implemented this interface.
    /// </summary>
    public interface IClosable
    {
        /// <summary>
        /// Occurs when <see cref="Window.Close()"/> invocation is requested from a ViewModel.
        /// </summary>
        event EventHandler<CloseRequestedEventArgs> CloseRequested;

        /// <summary>
        /// Raised just before <see cref="Window.Close()"/> invocation when it is requested from a ViewModel.
        /// </summary>
        /// <param name="sender">
        /// The source of the event.
        /// </param>
        /// <param name="e">
        /// An object that contains <see cref="CloseRequested"/> event data.
        /// </param>
        void OnClosing(object sender, CloseRequestedEventArgs e);
    }
}