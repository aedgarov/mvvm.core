﻿using Microsoft.Extensions.Logging;
using MVVM.Core.Actions;
using MVVM.Core.Services.ModalWindow;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Input;

namespace MVVM.Core.Base
{
    /// <summary>
    /// Provides base functionality for implementing MVVM pattern with modal windows showing/closing capabilities.
    /// </summary>
    public abstract class ModalViewModelBase : ViewModelBase, IClosable
    {
        /// <summary>
        /// Initializes a new instance of <see cref="ModalViewModelBase"/> class.
        /// </summary>
        protected ModalViewModelBase(ILogger logger = null)
            => CloseCommand = new Command(a => Close(Convert.ToBoolean(a, CultureInfo.InvariantCulture)), p => CanClose(), logger);

        /// <summary>
        /// Modal window title.
        /// </summary>
        public abstract string Title { get; set; }

        /// <summary>
        /// Invokes a <see cref="CloseRequested"/> event.
        /// </summary>
        public ICommand CloseCommand { get; }

        /// <summary>
        /// Raised just before <see cref="Window.Close()"/> invocation when it is requested from a ViewModel.
        /// </summary>
        /// <param name="sender">
        /// The source of the event.
        /// </param>
        /// <param name="e">
        /// An object that contains <see cref="CloseRequested"/> event data.
        /// </param>
        public virtual void OnClosing(object sender, CloseRequestedEventArgs e) { }

        /// <summary>
        /// Inidicates whether modal window can be closed from ViewModel or not.
        /// </summary>
        protected virtual bool CanClose()
            => true;

        /// <summary>
        /// Invokes a <see cref="CloseRequested"/> event.
        /// </summary>
        protected void Close(bool? dialogResult)
            => CloseRequested?.Invoke(this, new CloseRequestedEventArgs(dialogResult));

        /// <summary>
        /// Occurs when <see cref="Window.Close()"/> invocation is requested from a ViewModel.
        /// </summary>
        public event EventHandler<CloseRequestedEventArgs> CloseRequested;
    }
}