﻿namespace MVVM.Core.Services.ModalWindow
{
    /// <summary>
    /// Interface that mirrors some necessary properties of <see cref="Window"/> class
    /// to take control over <see cref="Window"/> instance that implements this interface.
    /// </summary>
    public interface IWindow : IModalWindow
    {
        /// <summary>
        /// Opens a window and returns without waiting for the newly opened window to close.
        /// </summary>
        void Show();
    }
}